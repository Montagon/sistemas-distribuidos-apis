def getImages(access_token, imagenes)
  if not access_token:
    return 'Missing Access Token'
  try:
    api = client.InstagramAPI(access_token=access_token,
      client_secret=CONFIG['client_secret'])
    recent_media, next = api.user_recent_media()
    listMedia = []
    cont = 0
    for media in recent_media:
      for img in imagenes:
        if(cont is int(img)):
          if(media.type == 'video'):
            listMedia.append(
            media.get_standard_resolution_url())
          else:
            listMedia.append(
            media.get_standard_resolution_url())
            cont = cont + 1
  except Exception as e:
  print(e)
  return listMedia
