def instagram2Dropbox(tokenInst, tokenDrop, n):
    images = getImages(tokenInst, n)
    client = dropbox_client(tokenDrop)
    for i in images:
        dropbox_upload(client, i, 'Fotos.jpg')
