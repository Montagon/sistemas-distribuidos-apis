/*jslint node: true */
/*jshint -W030 */
/*jshint -W106 */
/* global alert */
'use strict';

//creamos el modulo e inyectamos bootstrap ui
var app = angular.module('area', ['ngRoute']);


//damos configuración de ruteo a nuestro sistema de login
app.config(function($routeProvider)
{
    $routeProvider.when('/', {
        controller : 'controlador',
        templateUrl : 'instdropbox.html'
    })
    .when('/instaDropbox', {
        controller : 'controlador',
        templateUrl : 'instdropbox.html'
    })
    .when('/instaTwitter', {
        controller : 'controlador',
        templateUrl : 'insttwitter.html'
    })
    .when('/dropboxTwitter', {
        controller : 'controlador',
        templateUrl : 'dropboxtwitter.html'
    });
});

app.controller('controlador', function($scope) {
    //$scope.index = 3;
});

