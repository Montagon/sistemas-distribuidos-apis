/*jslint node: true */
/*global angular*/
/*global escape: true */
'use strict';

//Datos de la página principal
var seleccion = {};
var seleccionvideos = {};
var numalumnos = {};
var menucomollegar = [
    {
        'tipo': 'Coche',
        'icono': 'images/iconos/coche.png'
    },
    {
        'tipo': 'A pie',
        'icono': 'images/iconos/apie.png'
    },
    {
        'tipo': 'Autobús',
        'icono': 'images/iconos/autobus.png'
    }
];


var app = angular.module('miAplicacion', []);

//Controlador de la página principal.
app.controller('IndexController', ['$scope',
    function ($scope) {
        $scope.seleccion = seleccion;
        $scope.seleccionvideos = seleccionvideos;

        $scope.items = menucomollegar;        

        //Permite cambiar el transporte (Cómo llegar).
        $scope.onClick = function (s) {
            seleccion.mode = s;
            //console.log('Click registrado');
        };
    }
]);

app.controller('FormularioController', ['$scope', function ($scope) {
    var contactar = function (contacto) {
        console.log(contacto);
    };
        
}]);