/*jslint node: true */
/*global $*/
'use strict';

// Función jQuery para animar el scroll. Requiere el plugin "jQuery Easing".
$(function () {
    $('a.page-scroll').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Función para marcar la sección actual en la barra de navegación.
$(function () {
    $('body').scrollspy({
        target: '.navbar-fixed-top'
    });
});

//Función jQuery para ocultar la barra navegación y que aparezca al hacer scroll
$(function ($) {
    $(document).ready(function () {
        $('.navbar').hide(); //Oculta la barra
        $(function () { //Aparece la barra
            $(window).scroll(function () {
                if ($(this).scrollTop() > 150) {
                    $('.navbar').fadeIn();
                } else {
                    $('.navbar').fadeOut();
                }
            });
        });
    });
});