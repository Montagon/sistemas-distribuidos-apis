# -*- coding: utf-8 -*-
import bottle
from instagram import client, subscriptions
import beaker.middleware
CONFIG = {
        'client_id': '32f963a8ee1949cab634208efbd54cc6',
        'client_secret': '297c2cfaa34142f383d562f6cbc2bb2d',
        'redirect_uri': 'https://sistemas-distribuidos-apis-montagon.c9.io/instlog'
}

# obtención de la api
unauthenticated_api = client.InstagramAPI(**CONFIG)

def instagram_login():
    """ instagram_login
    Método para obtener acceso a la aplicacion.
    Precondición: La api de instagram ha de estar instanciada.
    Postcondición: Devuelve la url de atorización.
    """
    url = unauthenticated_api.get_authorize_url(scope=["likes","comments"])
    return url

def getImages(access_token, imagenes):
    """ getImages
    Método para obtener las ultimas publicaciones realizadas por el usuario.
    Precondición: token de acceso y listado de imágenes.
    Postcondición: Devuelve la lista con las imágenes descargadas.
    """
    if not access_token:
		return 'Missing Access Token'
    try:
		#Nos logueamos y obtenemos acceso
		api = client.InstagramAPI(access_token=access_token, client_secret=CONFIG['client_secret'])
		#Obtenemos los archivos multimedia
		recent_media, next = api.user_recent_media()
		listMedia = []
	    #Papara cada uno de ellos, los añadimos segun el tipo de media que sea
		cont = 0
		for media in recent_media:
			
			#Obtenemos solo el número de imágenes que nos han pedido
			for img in imagenes:
				if(cont is int(img)):
					if(media.type == 'video'):
						listMedia.append(media.get_standard_resolution_url())
					else:
						listMedia.append(media.get_standard_resolution_url())
			cont = cont + 1
    except Exception as e:
		print(e)
    return listMedia
