# -*- coding: utf-8 -*-
# Include the Dropbox SDK
from dropbox.client import DropboxOAuth2Flow, DropboxClient
from flask import session
import requests

def dropbox_login():
    """ dropbox_login
    Adquiere el token de auteticación OAuth.
    Postcondición: Devuelve el token.
    """
    app_key = 'popimere6jnv92j'
    app_secret = '450khaly5mn807h'
    redirect_uri = 'https://sistemas-distribuidos-apis-montagon.c9.io/droplog'
    return DropboxOAuth2Flow(app_key, app_secret, redirect_uri, session, 'dropbox-auth-csrf-token')

def dropbox_client(tokenDrop):
    """ dropbox_client
    Da acceso a la API de dropbox a partir de un token OAuth
    Precondición: tokenDrop debe de ser un token OAuth válido.
    Postcondición: Devuelve el cliente con acceso a la API de dropbox.
    """
    return DropboxClient(tokenDrop)

def dropbox_list(client,ruta='/'):
    """ dropbox_list
    Lista el directorio de dropbox seleccionado.
    Precondición: Cliente con acceso a la API y cadena con la ruta al directorio a listar.
    Postcondición: Imprime la información sobre el directorio.
    """
    folder_metadata = client.metadata(ruta)
    print "metadata:", folder_metadata

def dropbox_download(client,ruta, fichero):
    """ dropbox_download
    Descarga un fichero de un directorio de dropbox.
    Precondición: Cliente con acceso a la API, cadena con la ruta del directorio que contiene el 
    fichero y cadena con el nombre del fichero.
    Postcondición: Imprime la información sobre el fichero.
    """
    f, metadata = client.get_file_and_metadata(ruta+fichero)
    out = open(fichero, 'wb')
    out.write(f.read())
    out.close()
    print metadata

def dropbox_upload(client,ruta,fichero):
    """ dropbox_upload
    Sube un fichero a un directorio de dropbox.
    Precondición: Cliente con acceso a la API, ruta y fichero.
    Postcondición: Sube el fichero e imprime la respuesta del servicio.
    """
    r = requests.get(ruta)
    #f = urllib.urlopen(fichero, 'rb')
    response = client.put_file(fichero, r.content)
    print "uploaded:", response
