# -*- coding: utf-8 -*-
# Incluye la interfaz en python de la api de twitter
import twitter

def twitter_login():
    """ twitter_login
    Establece la sesión con la API de twitter.
    Postcondición: Devuelve el cliente con acceso a la API.
    """
    # Claves
    consumer_key = 'F41V0HFpPgFBQLAxYy8ziB6aj'
    consumer_secret = 'r3zHlr21KHcm8yknlGqgI7KA1aRdWerMNUjR8W3tERuJmIQuzw'
    access_token_key = '3258397893-fcfztJNKxFOFixD9zQPqOJlmkymmhjHUEoPfAzM'
    access_token_secret = 'lyIIPqRHQbSyV5qlcKDXAaucBFUvuPySRnXDvyHELoI0Z'

    # Cliente
    api = twitter.Api(consumer_key,consumer_secret,access_token_key,access_token_secret);
    
    return api

def twitter_post_tweet(api,text,image):
    """ twitter_post_tweet
    Postea una única imagen con un texto.
    Precondición: Cliente con acceso a la API, cadena con el texto a postear y 
    cadena con url de la imagen a subir.
    Postcondición: Devuelve el estado de la subida del tweet.
    """
    status = api.PostMedia(text,image)

    return status # Opcional

def twitter_post_tweets(api,text,images):
    """ twitter_post_tweets
    Postea varias imágenes, cada una en un tweet diferente, acompañadas con un texto.
    Precondición: Cliente con acceso a la API, cadena con el texto a postear y 
    lista de cadenas con las url de las imágenes a subir.
    Postcondición: Devuelve una lista con información sobre cada tweet subido.
    """
    status = []
    for im in images:
        status.append(api.PostMedia(text,images))

    return status # Opcional
