# -*- coding: utf-8 -*-
from api_dropbox import *
from api_instagram import *
from api_twitter import *

def instagram2Dropbox(tokenInst, tokenDrop, n):
    """ instagram2Dropbox
    Obtiene imágenes de instagram y las sube a dropbox.
    Precondición: token OAuth tanto de instagram como de dropbox y una lista de posiciones
    Postcondición: Sube las imágenes que descarga de instagram a dropbox.
    """
    images = getImages(tokenInst, n)
    client = dropbox_client(tokenDrop)
    for i in images:
        dropbox_upload(client, i, 'Fotos.jpg')
        
def instagram2Twitter(tokenInst, n):
    """ instagram2Twitter
    Obtiene imágenes de instagram y postea tweets con estas imágenes.
    Precondición: token OAuth de instagram y una lista de posiciones.
    Postcondición: Postea un tweet por cada imágen descargada de instagram.
    """
    images = getImages(tokenInst, n)
    api = twitter_login()
    cont = 1
    for i in images:
        twitter_post_tweet(api, "Foto " + str(cont), i)
        cont = cont + 1
