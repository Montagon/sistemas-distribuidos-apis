# -*- coding: utf-8 -*-
import os
from flask import Flask, render_template
from flask import request, redirect, session

import sys
sys.path.append('codigos/apis/')
from api_dropbox import *
from api_instagram import *
from api_twitter import *
from api_global import *

app = Flask(__name__, static_url_path='')

@app.route('/')
def hello():
    """ hello
    Índice de la aplicación. 
    Postcondición: Renderizado de la página principal.
    """
    return render_template('index.html')
    
    
@app.route('/instaLista')
def instaLista():
    """ instaLista
    Listado de imágenes de instagram para pasar a dropbox.
    Postcondición: Renderizado de la web.
    """
    if 'access_token_instagram' not in session:
        return redirect("/instagramLogin")
    else:
        if 'access_token_dropbox' not in session:
            return redirect("/dropboxLogin")
        else:
            imagenes = getImages(session['access_token_instagram'], range(0,20))
            return render_template('views/listaImagenesInst2Drop.html', imagenes=imagenes)
    
@app.route('/instaListaTwit')
def instaListaTwit():
    """ instaListaTwit
    Listado de imágenes de instagram para twittear.
    Postcondición: Renderizado de la web.
    """
    if 'access_token_instagram' not in session:
        return redirect("/instagramLogin")
    else:
        imagenes = getImages(session['access_token_instagram'], range(0,20))
        return render_template('views/listaImagenesInst2Twitter.html', imagenes=imagenes)
    
@app.route('/instaDropbox', methods=['POST'])
def instaDropbox():
    """ instaDropbox
    Función que recoge las imágenes seleccionadas y llama al método
    de la api unificado para descargarlas y subirlas a Dropbox.
    Postcondición: Devuelve la vista donde se explica el proceso.
    """
    value = request.form.getlist('imagenes') 
    instagram2Dropbox(session['access_token_instagram'], session['access_token_dropbox'], value)
    return render_template('views/ejemplos/instdropbox.html')
    
@app.route('/instaTwitter', methods=['POST'])
def instaTwitter():
    """ instaTwitter
    Función que recoge las imágenes seleccionadas y llama al método
    de la api unificado para descargarlas y publicarlas en Twitter.
    Postcondición: Devuelve la vista donde se explica el proceso.
    """
    value = request.form.getlist('imagenes')
    instagram2Twitter(session['access_token_instagram'], value)
    return render_template('views/ejemplos/insttwitter.html')


#---------------- AUTORIZACIÓN ------------------#
@app.route('/instagramLogin')
def instagramLogin():
    """ instagramLogin
    Función que redirige a la página para obtener la autorización de
    Instagram.
    """
    return redirect(instagram_login())
    
@app.route('/dropboxLogin')
def dropboxLogin():
    """ dropboxLogin
    Función que redirige a la página para obtener la autorización de
    Dropbox.
    """
    return redirect(dropbox_login().start())
    
#Si se loguea, volvemos a la pagina inicial
@app.route('/instlog')
def login():
    """ login
    Página de retorno de la petición de la autorización de la API de Instagram.
    Obtenemos el código y, en caso de que se encuentre, obtenemos el access token, 
    creamos la aplicación y guardamos en session el token de autorización.
    """
    code = request.args.get("code")
    if not code:
        return 'Missing code'
    try:
        access_token, user_info = unauthenticated_api.exchange_code_for_access_token(code)
        if not access_token:
            return 'Could not get access token'
        api = client.InstagramAPI(access_token=access_token, client_secret=CONFIG['client_secret'])
        session['access_token_instagram'] = access_token
    except Exception as e:
        print(e)
    return render_template('index.html')

@app.route('/droplog')
def loginDrop():
    """ login
    Página de retorno de la petición de la autorización de la API de Dropbox.
    Obtenemos el código y, en caso de que se encuentre, obtenemos el access token, 
    creamos la aplicación y guardamos en session el token de autorización.
    """
    try:
        access_token, user_id, url_state = dropbox_login().finish(request.args)
    except:
        abort(400)
    else:
        session['access_token_dropbox'] = access_token
    return render_template('index.html')

if __name__ == "__main__":
    app.debug = True
    app.secret_key = 'estoesunaclavesupersecreta'
    app.run(host=os.getenv('IP', '0.0.0.0'),port=int(os.getenv('PORT', 8080)))

